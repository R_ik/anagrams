import sys

assert len(sys.argv) == 2, "Wrong input. Usage: python3 anagram.py inputword"
input = sys.argv[1]
if len(input) > 20:
    print("Input length longer than 20 characters. This might take a long time")

def word_to_dict(word:str):
    d = {}
    for c in word:
        if c in d:
            d[c] += 1
        else:
            d[c] = 1
    return d

input_chars = word_to_dict(input)
##

def word_fits_input(word: str, input_chars: dict):
    chars_available = input_chars.copy()
    for c in word:
        if c in chars_available and chars_available[c] > 0:
            chars_available[c] -= 1
        else:
            return False
    return True

def subtract_word(word: str, chars: dict):
    for c in word:
        chars[c] -= 1
    return chars

def zero_left(chars: dict):
    for v in chars.values():
        if v != 0:
            return False
    return True

##
nl_words_filepath = "OpenTaal-210G-basis-gekeurd.txt"

with open(nl_words_filepath, 'r') as f:
    words = f.readlines()
    words = [word.strip() for word in words]

possible_words = [word for word in words if word_fits_input(word, input_chars)]
possible_words = [word for word in possible_words if len(word) > 2]
possible_words.append('de')

##
def sort_string(string):
    return ''.join(sorted(string))

ordered_words = {}
for word in possible_words:
    char_set = sort_string(word)
    if char_set not in ordered_words:
        ordered_words[char_set] = [word]
    else:
        ordered_words[char_set] += [word]

print(f"Found {len(ordered_words)} words that fit the input")
long_keys = list(ordered_words.keys())
long_keys.sort(key=lambda x: len(x), reverse=True)
n = min(20, len(long_keys))
for k in long_keys[:n]:
    print(f"{ordered_words[k]} ({len(k)}/{len(input)})")

##
import time
possible_char_sets = list(ordered_words.keys())
word_combinations = []
def find_anagrams(start_index, words_list, chars, depth):
    for word_idx in range(start_index, len(possible_char_sets)):
        if word_fits_input(possible_char_sets[word_idx], chars):
            new_words_list = words_list.copy()
            new_words_list.append(possible_char_sets[word_idx])
            new_chars = subtract_word(possible_char_sets[word_idx], chars.copy())
            find_anagrams(word_idx, new_words_list, new_chars, depth+1)
            if zero_left(new_chars):
                word_combinations.append(new_words_list)
                continue
start = time.perf_counter()
print("finding anagrams...")
find_anagrams(0, [], input_chars.copy(), 0)
end = time.perf_counter()

print(f"Found {len(word_combinations)} full anagrams in {end-start} seconds")

from itertools import product
expanded_word_combinations = [c for word_combination in word_combinations for c in product(*[ordered_words[key] for key in word_combination])]

##
expanded_word_combinations.sort(key=lambda x: len(x))
n = min(100, len(expanded_word_combinations))
print(f"Printing first {n} full anagram combinations")
for s in expanded_word_combinations[:n]:
    print(s)
with open("output/" + input + '.txt', 'w') as o:
    for sentence in expanded_word_combinations:
        o.write(",".join(sentence) + "\n")
print(f"Output written to output/{input}.txt")

##
